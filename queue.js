let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
   return collection;
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
    let count = collection.length;

    collection[count] = element;

    return collection;

}

function dequeue() {
    // In here you are going to remove the last element in the array

    let count = collection.length;

    let temp = collection[0];
    collection[0] = collection[count-1];
    collection[count-1] = temp;

    for(let i = 0; i<count; i++){
        if(i===count-1){
            collection.length = count-1;
        }
    }

    return collection;

}

function front() {
    // In here, you are going to remove the first element
    
    let name;
    let count = collection.length;

    let temp = collection[0];
    collection[0] = collection[count-1];
    collection[count-1] = temp;

    for(let i = 0; i<count; i++){
        if(i===count-1){
            name = collection[count-1];
            
        }
    }

    return name;
}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements  

    let count = 0;

    for(let i=0; i<10; i++){
        if(collection[i] !== undefined){
            count++;
        }else{
            break;
        }
    }

    return count;
    
}

function isEmpty() {
    //it will check whether the function is empty or not

    if(collection.length === 0){
        return true
    } else{
        return  false
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};